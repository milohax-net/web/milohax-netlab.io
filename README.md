# milohax.github.io

[milohax](https://sinewalker.keybase.pub/milohax.net/) [tiddlywiki](https://pages.github.com/) redirect page

[GitLab Pages instructions](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html)

[GitLab Custom Domain instructions](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/)

This exists to be a landing point for my domain, and redirect to wherever I'm hosting it (presently a public page on [keybase.io](https://keybase.io)
