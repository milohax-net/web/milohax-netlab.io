// -*- javascript -*-

// This is based on the code for the 404 page on milosophical.me

// Speed up, and skip countdown for direct meme references
var load_meme = window.location.href.includes('#');

var echo_delay = 22;
var line_delay = echo_delay * 2.8
var speedup = load_meme? 1.75: 1;
var countdown = 3;


var _text;
var text = [];
var TEXT = [
    [ '* login as: guest\n' ],
    [ '*Ok.\n' ],
    [ '*\n' ],
    [ ' *****************************************\n', line_delay ],
    [ ' ***                                   ***\n', line_delay ],
    [ ' ***     --ooOO MILOHAX.NET OOoo--     ***\n', line_delay ],
    [ ' ***                                   ***\n', line_delay ],
    [ ' *** Public memex of Mike J Lockhart   ***\n', line_delay ],
    [ ' ***                                   ***\n', line_delay ],
    [ ' *****************************************\n', line_delay ],
    [ '*\n' ],
    [ ' Initializing TiddlyWiki memex file\n' ],
    [ ' - upstream domain: ', 100 ],[ '*milohax-net.gitlab.io\n' ],
    [ ' - will download into local memory\n' ],
    [ ' \n', 200 ], 
    [ '    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n' ],
    [ '*     Notice: memex is more than 6 MB\n' ],
    [ '    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n' ],
    [ '*\n' ],
    [ 'Veryfying fingerprint:\n '], 
    [ '*BE:A7:67:F1:CF:09:18:41:91:92:10:EE:7B:40:9B:A4:4A:0C:A6:CE\n' ],
    [ '* ==> '],
    [ '*04:A4:B1:2C:5A:9F:FD:24:05:5F:60:57:61:07:F6:0B:2F:67:7A:AE\n' ],
    [ '*Ok.\n\n' ],
    [ '*READY' ],
    [ ' \n', 700 ]
];

function init(_text)
{
    for (var i = 0, m = _text.length; i < m; i++) {
        if (!_text[i][0]) break;

        // Use supplied delay
        if (_text[i][0][0] !== '*') {
            text.push(_text[i]);
            continue;
        }

        // Unless strings start with *, then emulate teletype
        var s = _text[i][0];
        for (var j = 1, mj = s.length; j < mj; j++) {
            text.push([s[j],echo_delay]);
        }
    }
    if (!load_meme) {
        var redir = 'Redirecting in '
        for (var r = 0; r < redir.length; r++) {
            text.push([redir[r], echo_delay]);
        }
        // Countdown ...
        for (var x = countdown; x>0; x--) {
            text.push([x.toString(),200]);
            for (var y = 4; y>0; y--) {
                text.push(['.',200]);
            }
        }
    }
    else {
        var redir = 'Jumping to meme!'
        for (var r = 0; r < redir.length; r++) {
            text.push([redir[r], echo_delay]);
        }
    }

    for (var x = 64; x > 0; x--) {
        text.push(['\n\n\n', line_delay/2]);
    }
}

$(window).load(function(){
    var $w = $('#wrapper');
    var $t = $('#terminal');
    var $c = $('#wrapper .cursor');
    $t.hide();
    $w.scrollTop($w[0].scrollHeight);

    $('#wrapper').click(function(){speedup = 1;});

    function show(i) {
      if (i == text.length) {
          setTimeout(function(){
            window.location.href
             = window.location.href.replace("milohax.net/","milohax-net.gitlab.io/web/tech-wiki/");
          }, 33);
      return;
    }

    var s = text[i][0];
    var delay = text[i][1];

    $t.show();
    $c.before(s);
    $w.scrollTop($w[0].scrollHeight);

    setTimeout(function(){
            show(i+1);
        }, Math.round((delay * (Math.random() * 0.5 + 0.75)) / speedup));
    }

    $w.addClass('loaded');
    setTimeout(function(){show(0)}, 0);
});

// Add event listener on keypress, to speed up the animation (ignore what the key is)
document.addEventListener('keypress', (event) => {
  var name = event.key;
  var code = event.code;
  speedup = 6;
}, false);
